# Tuncoin

A new Flutter project.

## Project Links
- **Link Backend without Blockchain:** _https://gitlab.com/mohamed.testouri/Tuncoin-Backend_
- **Link Backend with Blockchain:** _https://gitlab.com/mohamed.testouri/Tuncoin_
- **Link Mobile Appliacation:** : _https://gitlab.com/espritmobile-2021/enigma-team/tuncoin-mobile_
- **Link Frontend Web:** _https://gitlab.com/espritmobile-2021/enigma-team/tuncoin-webfront_
- **APK:** _https://drive.google.com/file/d/178ieD5R9B-FbXDYPDvPQ79b_mxgzI4uA/view?usp=sharing_

### Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
